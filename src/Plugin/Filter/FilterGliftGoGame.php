<?php

namespace Drupal\glift_go_game\Plugin\Filter;

use Drupal\filter\FilterProcessResult;
use Drupal\filter\Plugin\FilterBase;

/**
 * @Filter(
 *   id = "filter_glift",
 *   title = @Translation("Glift Go Game Filter"),
 *   description = @Translation("Embed and view SGF files with go game records in content using Glift js."),
 *   type = Drupal\filter\Plugin\FilterInterface::TYPE_TRANSFORM_IRREVERSIBLE,
 * )
 */

class FilterGliftGoGame extends FilterBase {

  public function process($text, $langcode) {
    // Find, using regexp, the glift start and end tags. Send this to renderer.
    $rendering = preg_replace_callback('|\[glift\](.+)\[/glift\]|', array(get_class($this),'glift_renderer'), $text);
    $result = new FilterProcessResult($rendering);
    return $result;
  }

  public static function glift_renderer($matches) {
    // ASSUMPTION: An SGF is only included once at a given page with exactly the 
    // same settings. This is not needed in theory, but currently the divID for 
    // Glift is named by the hash of each input string to glift, meaning that 
    // if two exactly identical strings are given on the same page the divs 
    // will be called the same.
    // as usual: $matches[0] is the complete match
    // $matches[1] the match for the first subpattern
    // enclosed in '(...)'. So, matches[1] contains arguments to glift.
    $input = explode(" ", $matches[1]);
    
    // Create id (unique alphanumeric string) for this SGF data.
    $divid = 'glift_' . hash('md4', $matches[1]);
    
    // Set default key-value pairs for rendering
    $args = array();
    $args["sgf"] = '';
    $args["divId"] = $divid;
    $args["theme"] = 'DEFAULT';
    $args["disableZoomForMobile"] = 'true';
    $args["drawBoardCoords"] = 'true';
    
    if (count($input)==1){
      // Argument to glift is a single string. Assume this is raw SGF data or raw URL. 
      // Tags like sgf= are dealt with below.
      $args["sgf"] = $input[0];
    }
    // Else, we have many arguments. Go through each
    else {
      # Modify default arguments from supplied parameters
      foreach ($input as $arg) {
        $keyval = explode("=",$arg);
        $key = $keyval[0];
        $key = str_replace('"', "", $key);
        $key = str_replace("'", "", $key);
        $value = $keyval[1];
        # Make sure option value is stored without quotes, these are added later
        $value = str_replace('"', "", $value);
        $value = str_replace("'", "", $value);
        # Replace default argument with given value
        $args[$key] = $value;
        }
    }
    
    //print_r($args); // DEBUG
    
    // Build output html/javascript
    $output = "<div id='$divid' style='height:500px; width:100%; position:relative;'></div>";
    $output .= "<script type='text/javascript'>gliftWidget = glift.create({";
    $output .= "sgf: '" . $args["sgf"] . "',";
    $output .= "divId: '" . $args["divId"] . "',";
    $output .= "display: {";
    $output .= "  theme: '" . $args["theme"] . "',";
    $output .= "  disableZoomForMobile: " . $args["disableZoomForMobile"] . ",";
    $output .= "  drawBoardCoords: " . $args["drawBoardCoords"] . ",";
    $output .= "}" ;
    $output .= "});</script>" ;
    $output .= "<div align ='center'><noscript>Please enable JavaScript to view this game.</noscript>";
    $output .= "<a type='application/x-go-sgf' href=' " . $args["sgf"] . "' download>Download SGF</a></div>";
    return $output;
  }
}
